from django.urls import path

from . import views

app_name = 'storyprofile'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('gallery/', views.gallery, name='gallery')
]