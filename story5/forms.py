from django import forms

class FormMatkul(forms.Form):
    mata_kuliah = forms.CharField(max_length=100)
    dosen = forms.CharField(max_length=100)
    sks = forms.CharField(max_length=1)
    deskripsi = forms.CharField(
        widget = forms.Textarea
    )
    semester = forms.CharField(max_length=60)
    ruang = forms.CharField(max_length=60)
