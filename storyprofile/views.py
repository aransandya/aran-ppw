from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'storyprofile/profile.html')

def gallery(request):
    return render(request, 'storyprofile/gallery.html')