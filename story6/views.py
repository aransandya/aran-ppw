from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import FormOrang, FormAktivitas
from .models import ModelOrang, ModelAktivitas

def indexAktivitas(request):
    return render(request, 'story6/indexaktivitas.html')

def daftarAktivitas(request):
    form_aktivitas = FormAktivitas()
    if request.method == 'POST':
        input_form_aktivitas = FormAktivitas(request.POST)
        if input_form_aktivitas.is_valid():
            ModelAktivitas.objects.create(
                nama_aktivitas = input_form_aktivitas.cleaned_data.get('nama_aktivitas'),
                deskripsi_aktivitas = input_form_aktivitas.cleaned_data.get('deskripsi_aktivitas'),
            )
            return HttpResponseRedirect("/story-6/list-aktivitas/")
    context = {
        'form_aktivitas':form_aktivitas
    }
    return render(request,'story6/daftaraktivitas.html', context)

def listAktivitas(request):
    aktivitass = ModelAktivitas.objects.all()
    orangs = ModelOrang.objects.all()
    context = {
        'aktivitass':aktivitass,
        'orangs':orangs,
    }
    return render(request, 'story6/listaktivitas.html', context)

def deleteOrang(request, id_delete):
    try:
        delete_orang = ModelOrang.objects.filter(id=id_delete)
        delete_orang.delete()
        return redirect('story6:listAktivitas')
    except:
        return redirect('story6:listAktivitas')

def daftarOrang(request, id_daftar):
    form_orang = FormOrang()
    if request.method == 'POST':
        input_form_orang = FormOrang(request.POST)
        if input_form_orang.is_valid():
            data = input_form_orang.cleaned_data
            objek_orang = ModelOrang()
            objek_orang.nama_orang = data['nama_orang']
            objek_orang.aktivitas = ModelAktivitas.objects.get(id=id_daftar)
            objek_orang.save()
            return HttpResponseRedirect("/story-6/list-aktivitas/")
    context = {
        'form_orang':form_orang
    }
    return render(request,'story6/daftarorang.html', context)
