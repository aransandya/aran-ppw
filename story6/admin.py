from django.contrib import admin

# Register your models here.

from .models import ModelOrang, ModelAktivitas

admin.site.register(ModelOrang)
admin.site.register(ModelAktivitas)