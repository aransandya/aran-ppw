from django.apps import AppConfig


class StoryprofileConfig(AppConfig):
    name = 'storyprofile'
