from django.test import TestCase, Client
from .models import ModelOrang, ModelAktivitas
from .forms import FormOrang, FormAktivitas
from .views import indexAktivitas, daftarAktivitas, listAktivitas, deleteOrang, daftarOrang
from django.urls import resolve, reverse
from .apps import Story6Config
from django.apps import apps

# Create your tests here.
class TestModel(TestCase):
    def setUp(self):
        self.model_orang = ModelOrang.objects.create(
            nama_orang = "Aransandya"
        )
        self.model_aktivitas = ModelAktivitas.objects.create(
            nama_aktivitas = "menonton konser",
            deskripsi_aktivitas = "menonton konser DAY6"
        )
    
    def test_instance_is_created(self):
        self.assertEqual(ModelOrang.objects.count(), 1)
        self.assertEqual(ModelAktivitas.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.model_aktivitas), "menonton konser")
        self.assertEqual(str(self.model_orang), "Aransandya")


class TestUrls(TestCase):
    def setUp(self):
        self.client = Client()

    def test_is_url_index_aktivitas_available(self):
        response = Client().get('/story-6/index-aktivitas/')
        self.assertEquals(response.status_code, 200)

    def test_is_url_daftar_aktivitas_available(self):
        response = Client().get('/story-6/daftar-aktivitas/')
        self.assertEquals(response.status_code, 200)
    
    def test_is_url_list_aktivitas_available(self):
        response = Client().get('/story-6/list-aktivitas/')
        self.assertEquals(response.status_code, 200)
    
    def test_is_url_daftar_orang_available(self):
        response = Client().get('/story-6/daftar-orang/P1/')
        self.assertEquals(response.status_code, 200)


class TestViews(TestCase):
    def setUp(self):
        self.indexAktivitas = reverse("story6:indexAktivitas")
        self.daftarAktivitas = reverse("story6:daftarAktivitas")
        self.listAktivitas = reverse("story6:listAktivitas")

    def test_GET_index_aktivitas(self):
        response = self.client.get(self.indexAktivitas)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/indexaktivitas.html')

    def test_GET_list_aktivitas(self):
        response = self.client.get(self.listAktivitas)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/listaktivitas.html')

    def test_GET_daftar_aktivitas(self):
        response = self.client.get(self.daftarAktivitas)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/daftaraktivitas.html')

    def test_POST_daftar_aktivitas(self):
        response = self.client.post(self.daftarAktivitas,{'nama': 'sandya', 'deskripsi': 'minum'}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_daftar_aktivitas_invalid(self):
        response = self.client.post(self.daftarAktivitas,{'nama': '', 'deskripsi': ""}, follow = True)
        self.assertTemplateUsed(response, 'story6/daftaraktivitas.html')

    def test_GET_delete(self):
        model_aktivitas = ModelAktivitas(nama_aktivitas="makan", deskripsi_aktivitas="makan nasi")
        model_aktivitas.save()
        model_orang = ModelOrang(nama_orang="siapa",
            aktivitas=ModelAktivitas.objects.get(id=1))
        model_orang.save()
        response = self.client.get(reverse('story6:deleteOrang', args=[model_orang.id]))
        self.assertEqual(ModelOrang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)



class TestHTML(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index_aktivitas_templates_is_used(self):
        response = Client().get('/story-6/index-aktivitas/')
        self.assertTemplateUsed(response, 'story6/indexaktivitas.html')

    def test_daftar_aktivitas_templates_is_used(self):
        response = Client().get('/story-6/daftar-aktivitas/')
        self.assertTemplateUsed(response, 'story6/daftaraktivitas.html')
    
    def test_list_aktivitas_templates_is_used(self):
        response = Client().get('/story-6/list-aktivitas/')
        self.assertTemplateUsed(response, 'story6/listaktivitas.html')
    
    def test_daftar_orang_templates_is_used(self):
        response = Client().get('/story-6/daftar-orang/P1/')
        self.assertTemplateUsed(response, 'story6/daftarorang.html')


class TestForms(TestCase):
    def test_form_is_valid(self):
        form_orang = FormOrang(data={
            'nama_orang':'Aransandya'
        })
        self.assertTrue(form_orang.is_valid())
        form_aktivitas = FormAktivitas(data={
            'nama_aktivitas':'menonton konser',
            'deskripsi_aktivitas':'menonton konser DAY6'
        })
        self.assertTrue(form_aktivitas.is_valid())

    def test_form_invalid(self):
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())
        form_aktivitas = FormAktivitas(data={})
        self.assertFalse(form_aktivitas.is_valid())

class TestDaftarOrang(TestCase):
    def setUp(self):
        model_aktivitas = ModelAktivitas(nama_aktivitas="makan", deskripsi_aktivitas="makan nasi")
        model_aktivitas.save()

    # def test_daftar_orang_POST(self):
    #     response = Client().post('/story-6/daftar-orang/P4/',
    #         data={'nama_orang': 'aran'
    #     })
    #     self.assertEqual(response.status_code, 200)

    def test_daftar_orang_GET(self):
        response = self.client.get('/story-6/daftar-orang/P4/')
        self.assertTemplateUsed(response, 'story6/daftarorang.html')
        self.assertEqual(response.status_code, 200)

    def test_daftar_orang_POST_invalid(self):
        response = Client().post('/story-6/daftar-orang/P1/',
            data={'nama': ''
        })
        self.assertTemplateUsed(response, 'story6/daftarorang.html')
        self.assertEqual(response.status_code, 200)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')
