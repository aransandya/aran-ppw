from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import FormMatkul
from .models import MatkulModel

def matkul(request):
    return render(request, 'story5/matkul.html')

def isi(request):
    form_matkul = FormMatkul(request.POST or None)

    if request.method == 'POST':
        if form_matkul.is_valid():
            MatkulModel.objects.create(
                mata_kuliah = form_matkul.cleaned_data.get('mata_kuliah'),
                dosen = form_matkul.cleaned_data.get('dosen'),
                sks = form_matkul.cleaned_data.get('sks'),
                deskripsi = form_matkul.cleaned_data.get('deskripsi'),
                semester = form_matkul.cleaned_data.get('semester'),
                ruang = form_matkul.cleaned_data.get('ruang'),
            )
            return HttpResponseRedirect("/story-5/daftar/")

    context = {
        'title':'Daftarkan Jadwal',
        'form_matkul':form_matkul
    }
    return render(request,'story5/isi.html', context)

def daftar(request):
    matkuls = MatkulModel.objects.all()
    context = {
        'title':'Daftar Mata Kuliah',
        'matkuls':matkuls,
    }
    return render(request, 'story5/daftar.html', context)

def detail(request, id_matkul):
    matkul = MatkulModel.objects.filter(id=id_matkul).get(id=id_matkul)
    response = {
        'matkul':matkul,
    }
    return render(request, 'story5/detailmatkul.html', response)

def delete(request, id_delete):
    try:
        delete_jadwal = MatkulModel.objects.filter(id=id_delete)
        delete_jadwal.delete()
        return redirect('story5:daftar')
    except:
        return redirect('story5:daftar')