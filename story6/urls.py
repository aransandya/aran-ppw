from django.urls import path, include

from . import views

app_name = 'story6'

urlpatterns = [
    path('index-aktivitas/', views.indexAktivitas, name='indexAktivitas'),
    path('daftar-aktivitas/', views.daftarAktivitas, name='daftarAktivitas'),
    path('list-aktivitas/', views.listAktivitas, name='listAktivitas'),
    path('delete/P<int:id_delete>/', views.deleteOrang, name='deleteOrang'),
    path('daftar-orang/P<int:id_daftar>/', views.daftarOrang, name='daftarOrang'),
]
