from django.db import models

# Create your models here.
class ModelAktivitas(models.Model):
    nama_aktivitas = models.CharField(max_length=100)
    deskripsi_aktivitas = models.TextField(null=True)

    def __str__ (self):
        return self.nama_aktivitas

class ModelOrang(models.Model):
    nama_orang = models.CharField(max_length=100)
    aktivitas = models.ForeignKey(ModelAktivitas, on_delete=models.CASCADE, null=True, blank=True)

    def __str__ (self):
        return self.nama_orang  