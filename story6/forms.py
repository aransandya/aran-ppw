from django import forms

class FormOrang(forms.Form):
    nama_orang = forms.CharField(max_length=100)

class FormAktivitas(forms.Form):
    nama_aktivitas = forms.CharField(max_length=100)
    deskripsi_aktivitas = forms.CharField(
        widget = forms.Textarea
    )