from django.db import models

# Create your models here.

class MatkulModel(models.Model):
    mata_kuliah = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.CharField(max_length=1)
    deskripsi = models.TextField(null=True)
    semester = models.CharField(max_length=60)
    ruang = models.CharField(max_length=60)

