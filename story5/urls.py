from django.urls import path, include

from . import views

app_name = 'story5'

urlpatterns = [
    path('matkul/', views.matkul, name='matkul'),
    path('isi/', views.isi, name='isi'),
    path('daftar/', views.daftar, name='daftar'),
    path('delete/P<int:id_delete>/', views.delete, name='delete'),
    path('detail/P<int:id_matkul>/', views.detail, name='detail'),
]
