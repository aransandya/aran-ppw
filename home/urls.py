from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('story-1/', views.index, name='index'),
]